<?php

namespace herb\PrepaBundle\Controller;

use herb\AdminBundle\Entity\Preparation; //maybe change this location
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PrepaController extends Controller
{
    public function indexAction() {
        $repository = $this->getDoctrine()->getRepository('herbAdminBundle:Preparation');
        $preparations_get_from_db = $repository
                                    ->createQueryBuilder('p')
                                    ->andWhere('p.isPublished = 1')
                                    ->addOrderBy('p.titre', 'ASC')
                                    ->getQuery()
                                    ->execute()
                                ;
        return $this->render('@herbPrepa/index.html.twig', array('preparations' => $preparations_get_from_db));
    }

    public function viewAction(Preparation $preparation) {
        return $this->render('@herbPrepa/view.html.twig', array('preparation' => $preparation));
    }
}
