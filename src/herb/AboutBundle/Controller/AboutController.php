<?php

namespace herb\AboutBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AboutController extends Controller
{
    public function indexAction()
    {
        return $this->render('@herbAbout/index.html.twig');
    }
}
