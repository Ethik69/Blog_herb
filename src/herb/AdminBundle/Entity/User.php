<?php

namespace herb\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;


/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity()
 * @ORM\Entity(repositoryClass="herb\AdminBundle\Repository\UserRepository")
 * @UniqueEntity(fields="username", message="That username is taken!")
 * @UniqueEntity(fields="email", message="That email is taken!")
 */
class User implements UserInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="Give us at least 3 characters")
     * @Assert\Length(min=3, minMessage="Give us at least 3 characters!")
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=64)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="json_array")
     */
    private $roles = array();

    /**
     * @Assert\NotBlank
     * @Assert\Regex(
     *      pattern="/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/",
     *      message="Use 1 upper case letter, 1 lower case letter, and 1 number"
     * )
     */
    private $plainPassword;

    public function eraseCredentials() {
        $this->setPlainPassword(null);
    }

    public function getId() {
        return $this->id;
    }

    public function setUsername($username) {
        $this->username = $username;

        return $this;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setPlainPassword($password) {
        $this->plainPassword = $password;

        return $this;
    }

    public function getPlainPassword() {
        return $this->plainPassword;
    }

    public function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    public function getPassword() {
        return $this->password;
    }

    public function encodePassword(PasswordEncoderInterface $encoder) {
      if ($this->plainPassword) {
        $this->salt = sha1(uniqid(mt_rand()));
        $this->password = $encoder->encodePassword($this->plainPassword, $this->salt);

        $this->eraseCredentials();
      }
    }

    public function getSalt() {
        return $this->salt;
    }

    public function setRoles(array $roles) {
        $this->roles = $roles;

        return $this;
    }

    public function getRoles() {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function __toString() {
      return (string) $this->getUsername();
    }

}
