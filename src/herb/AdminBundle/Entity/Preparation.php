<?php

namespace herb\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Preparation
 * @ORM\Entity
 * @ORM\Table(name="preparation")
 */
class Preparation
{
  /**
  * @ORM\Column(type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  private $id;
  /**
  * @ORM\Column(type="string", length=255)
  */
  private $titre;
  /**
  * @ORM\Column(type="string")
  */
  private $vignette;
  /**
  * @ORM\Column(type="text")
  */
  private $description;
  /**
  * @ORM\Column(type="text")
  */
  private $contenu;
  /**
  * @ORM\Column(type="boolean")
  */
  private $isPublished = false;
  /**
  * @ORM\Column(type="datetime")
  */
  private $date;

  public function __construct() {
    $this->date = new \DateTime('NOW');
  }

  public function getId() {
    return $this->id;
  }

  public function getVignette() {
    return $this->vignette;
  }

  public function setVignette($vignette) {
    $this->vignette = $vignette;

    return $this;
  }
  public function getIsPublished() {
    return $this->isPublished;
  }

  public function setIsPublished($isPublished) {
    $this->isPublished = $isPublished;

    return $this;
  }

  public function getTitre() {
    return $this->titre;
  }

  public function setTitre($titre) {
    $this->titre = $titre;

    return $this;
  }

  public function getDescription() {
    return $this->description;
  }

  public function setDescription($description) {
    $this->description = $description;

    return $this;
  }

  public function getContenu() {
    return $this->contenu;
  }

  public function setContenu($contenu) {
    $this->contenu = $contenu;

    return $this;
  }

  public function setDate($date) {
      $this->date = $date;

      return $this;
  }

  public function getDate() {
      return $this->date;
  }
}
