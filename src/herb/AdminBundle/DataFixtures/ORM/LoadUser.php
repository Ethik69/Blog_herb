<?php

namespace herb\AdminBundle\DataFixtures\ORM;

use herb\AdminBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Defines the sample data to load in the database when running the unit and
 * functional tests. Execute this command to load the data:
 *
 *   $ php bin/console doctrine:fixtures:load
 *
 * See http://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
 */

class LoadUser implements FixtureInterface, ContainerAwareInterface
{
  private $container;

  public function load(ObjectManager $manager) {
      $this->loadUsers($manager);
  }

  private function loadUsers(ObjectManager $manager) {
      $user = new User();
      $user->setUsername('ethan');
      $user->setPlainPassword('ThisIsThePassWord69$');

      $factory = $this->container->get('security.encoder_factory');
      $encoder = $factory->getEncoder($user);
      $user->encodePassword($encoder);

      $manager->persist($user);
      $manager->flush();
  }

  public function setContainer(ContainerInterface $container = null) {
      $this->container = $container;
  }
}
