<?php

namespace herb\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class PreparationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
        ->add('titre',       TextType::class, array('required' => false))
        ->add('vignette',       FileType::class,  array('data_class' => null, 'required' => false))
        ->add('description', TextareaType::class, array('required' => false))
        ->add('isPublished', CheckboxType::class, array('required' => false))
        ->add('date',        DateType::class, array('required' => false))
        ->add('contenu',     TextareaType::class, array('required' => false))
        ->add('Envoyer',     SubmitType::class)
      ;

    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'herb\AdminBundle\Entity\Preparation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'herb_adminbundle_preparation';
    }


}
