<?php

namespace herb\AdminBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader {

    private $targetDirectory;

    public function __construct($targetDirectory) {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file, $fileName, $folder) {
        $fileName = $fileName.'-'.$this->generateUniqueFileName().'.'.$file->guessExtension();

        // move file to the right directory
        $file->move(
          $this->getTargetDirectory().$folder,
          utf8_decode($fileName)
        );

        return $fileName;
    }

    public function getTargetDirectory() {
        return $this->targetDirectory;
    }

    public function generateUniqueFileName() {
      return md5(uniqid());
    }

}
