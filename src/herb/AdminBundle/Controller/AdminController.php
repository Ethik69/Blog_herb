<?php

namespace herb\AdminBundle\Controller;

use herb\AdminBundle\Entity\Preparation;
use Symfony\Component\DomCrawler\Crawler;
use herb\AdminBundle\Form\PreparationType;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class AdminController extends Controller
{

    public function loginAction() {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
          return $this->redirectToRoute('herb_admin_homepage');
        }

        $authenticationUtils = $this->get('security.authentication_utils');

        return $this->render('@herbAdmin/login.html.twig', array(
          'last_username' => $authenticationUtils->getLastUsername(),
          'error'         => $authenticationUtils->getLastAuthenticationError(),
        ));
    }

    public function indexAction() {
        return $this->render('@herbAdmin/index.html.twig');
    }

    // function prefixed by "art" are for all the articles managment

    public function artcreaAction() {
        return $this->render('@herbAdmin/article_creation.html.twig');
    }

    // function prefixed by "pre" are for all the preparations managment

    public function precreaAction(Request $request) {
        $preparation = new Preparation();
        $form = $this->createForm(PreparationType::class, $preparation);

        if ($request->isMethod('POST')) {
          $form->handleRequest($request);
          if ($form->isValid()) {

            // get file content
            $file = $preparation->getVignette();

            // get fileUpload service
            $fileUploader = $this->container->get('Admin.FileUploader');
            // save file name instead of file content (upload file and return name)
            // params upload(file, fileNamePrefix, folder)
            $fileName = $fileUploader->upload($file, 'preparation-'.$preparation->getTitre(), '/preparation');
            $preparation->setVignette($fileName);

            // recup entity manager
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($preparation);
            $manager->flush();

            $this->addFlash('notice', 'Formulaire bien envoyer.');
            return $this->render('@herbAdmin/prepa_creation.html.twig',
                                 array('form' => $form->createView()));

          } else {
            $this->addFlash('notice', 'Formulaire non valide !');
            return $this->render('@herbAdmin/prepa_creation.html.twig',
                                 array('form' => $form->createView()));
          }
        }

        return $this->render('@herbAdmin/prepa_creation.html.twig',
                             array('form' => $form->createView()));
    }

    public function prelistAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('herbAdminBundle:Preparation');
        $preparations_get_from_db = $repository
                                        ->createQueryBuilder('p')
                                        ->addOrderBy('p.titre', 'ASC')
                                        ->getQuery()
                                        ->execute()
                                    ;

        return $this->render('@herbAdmin/prepa_list.html.twig', array('preparations' => $preparations_get_from_db));
    }

    public function preeditAction(Preparation $preparation, Request $request) {
        // if isset (fileinput) del + create new

        $form = $this->createForm(PreparationType::class, $preparation);
        $old_vignette = $preparation->getVignette();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {

                // get file content
                $file = $preparation->getVignette();

                if ($file === null) {
                    $preparation->setVignette($old_vignette);

                } else {
                    // remove vignette
                    $fileSystem = new Filesystem();

                    try {
                        $fileSystem->remove($this->getParameter('images_directory').'/preparation/'.$old_vignette);
                    } catch (IOExceptionInterface $exception) {
                        $this->addFlash('notice', "An error occurred while removing your vignette at ".$exception->getPath());
                    }

                    // get file name
                    // get fileUpload service
                    $fileUploader = $this->container->get('Admin.FileUploader');
                    // save file name instead of file content (upload file and return name)
                    // params upload(file, fileNamePrefix, folder)
                    $fileName = $fileUploader->upload($file, 'preparation-'.$preparation->getTitre(), '/preparation');

                    // save file name instead of file content
                    $preparation->setVignette($fileName);
                }

                // recup entity manager
                $manager = $this->getDoctrine()->getManager();
                // put data in the db
                $manager->flush();

                $this->addFlash('notice', 'Formulaire bien envoyer.');
                return $this->render('@herbAdmin/prepa_creation.html.twig',
                                     array('form' => $form->createView(),
                                           'preparation' => $preparation));

            } else {
                $this->addFlash('notice', 'Formulaire non valide !');
                return $this->render('@herbAdmin/prepa_creation.html.twig',
                                     array('form' => $form->createView(),
                                           'preparation' => $preparation));
            }
        }

        return $this->render('@herbAdmin/prepa_creation.html.twig',
                             array('form' => $form->createView(),
                                   'preparation' => $preparation));
    }

    public function predelAction(Preparation $preparation, Request $request) {
        // remove vignette
        $fileSystem = new Filesystem();

        try {
            $fileSystem->remove($this->getParameter('images_directory').'/preparation/'.$preparation->getVignette());
        } catch (IOExceptionInterface $exception) {
            $this->addFlash('notice', "An error occurred while removing your vignette at ".$exception->getPath());
        }

        $images_urls = array();
        array_push($images_urls, $this->parse_html($preparation->getDescription()));
        array_push($images_urls, $this->parse_html($preparation->getContenu()));

        $this->remove_images($images_urls);

        // get manager then remove $preparation from db
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($preparation);
        $manager->flush();

        $this->addFlash('notice', 'Préparation bien supprimée.');
        return $this->prelistAction($request);
    }

    // ---------------------------------------------------------------------------

    public function parse_html($html) {
        // return all image src
        $crawler = new Crawler();
        $crawler->addHTMLContent($html);
        $images_urls = array();

        for ($i=0;$i<$crawler->filter('img')->count();$i++) {
            array_push($images_urls, $crawler->filter('img')->eq($i)->attr('src'));
        }

        return $images_urls;
    }

    public function remove_images($images_urls) {
        // del image from given urls
        $fileSystem = new Filesystem();

        foreach ($images_urls[0] as $img) {
            $img = explode('/', $img);
            $fileSystem->remove($this->getParameter('images_directory').'/'.end($img));
        }
    }
}
