<?php

namespace herb\ContactBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

class Contact
{
  /**
  *  @Assert\Length(min=3, max=100)
  *  @Assert\NotBlank()
  */
  private $sujet;
  /**
  *  @Assert\Length(min=3, max=50)
  *  @Assert\NotBlank()
  */
  private $nom;
  /**
  *  @Assert\Length(min=10)
  *  @Assert\NotBlank()
  *  @Assert\Email()
  */
  private $email;
  /**
  *  @Assert\Length(min=10)
  *  @Assert\NotBlank()
  */
  private $message;

  public function getSujet() {
    return $this->sujet;
  }

  public function setSujet($sujet) {
    $this->sujet = $sujet;

    return $this;
  }

  public function getNom() {
    return $this->nom;
  }

  public function setNom($nom) {
    $this->nom = $nom;

    return $this;
  }

  public function getEmail() {
    return $this->email;
  }

  public function setEmail($email) {
    $this->email = $email;

    return $this;
  }

  public function getMessage() {
    return $this->message;
  }

  public function setMessage($message) {
    $this->message = $message;

    return $this;
  }
}
