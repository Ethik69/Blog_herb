<?php

namespace herb\ContactBundle\Controller;

use herb\ContactBundle\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;


class ContactController extends Controller
{
    public function testAction()
    {
        return $this->render('@herbContact/index.html.twig');
    }

    public function indexAction(Request $request)
    {
      $contact = new Contact();

      // Creation du formulaire
      $formBuilder = $this->get('form.factory')->createBuilder(FormType::class,
                                                               $contact);

      // Ajout des champs
      $formBuilder
        ->add('Sujet',    TextType::class)
        ->add('Nom',   TextType::class)
        ->add('Email',    EmailType::class)
        ->add('Message',  TextareaType::class)
        ->add('Envoyer',     SubmitType::class)
      ;

      // Recuperation du formulaire contruit
      $form = $formBuilder->getForm();

      // Gestion l'ors de l'envoi du formulaire
      if ($request->isMethod('POST')) {
        //recupération des données
        $form->handleRequest($request);
        // Test si le formulaire a bien été rempli
        if ($form->isValid()) {

          $message = (new \Swift_Message('Formulaire Contact Blog'))
              ->setFrom('ethan.chamik@gmail.com')
              ->setTo('ethancmk@hotmail.fr')
              ->setBody('Nom: '.$contact->getNom().'<br><br>Email: '.$contact->getEmail().'<br><br>Sujet: '.$contact->getSujet().'<br><br>Message: '.$contact->getMessage()
                       , 'text/html')
          ;

          $this->get('mailer')->send($message);

          $request->getSession()->getFlashBag()->add('notice', 'Formulaire correctement envoyé.');
          return $this->render('@herbContact/index.html.twig',
                               array('form' => $form->createView()));
        }
      }

      $request->getSession()->getFlashBag()->add('notice', 'Vous pouvez utiliser ce formulaire pour me contacter.');
      return $this->render('@herbContact/index.html.twig',
                           array('form' => $form->createView()));
    }
}
