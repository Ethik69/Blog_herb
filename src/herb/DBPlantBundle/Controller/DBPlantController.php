<?php

namespace herb\DBPlantBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DBPlantController extends Controller
{
    public function indexAction()
    {
        return $this->render('@herbDBPlant/index.html.twig');
    }
}
