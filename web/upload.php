<?php
  $imageFolder = "assets/images/";

  reset($_FILES);
  $temp = current($_FILES);
  if (is_uploaded_file($temp['tmp_name'])){

    // Sanitize input
    if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
        header("HTTP/1.1 400 Invalid file name.");
        return;
    }

    // Verify extension
    if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))) {
        header("HTTP/1.1 400 Invalid extension.");
        return;
    }

    // replace all space by _
    $temp['name'] = str_replace(" ", "_", $temp['name']);

    if (strpos($temp['name'], 'preparation') !== false) {
      $filetowrite = $imageFolder.'preparation/'.$temp['name'];
    } else {
      $filetowrite = $imageFolder.$temp['name'];
    }

    // Accept upload
    move_uploaded_file($temp['tmp_name'], $filetowrite);

    // Respond to the successful upload with JSON.
    // Use a location key to specify the path to the saved image resource.
    // { location : '/your/uploaded/image/file'}
    echo json_encode(array('location' => "/blog_herb/web/".$filetowrite));

  } else {
    // Notify editor that the upload failed
    header("HTTP/1.1 500 Server Error");
  }
?>
